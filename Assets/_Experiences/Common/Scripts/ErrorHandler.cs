﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SAE
{
	public class ErrorHandler
	{
		private static bool logHandlerRegistered = false;

		[RuntimeInitializeOnLoadMethod]
		public static void SetupExceptionHandling()
		{
			// only attempt to register if it has not already been registered
			if (!logHandlerRegistered)
			{
				logHandlerRegistered = true;
				Application.logMessageReceived += LogEventHandler;
			}
		}

		static void LogEventHandler(string condition, string stackTrace, LogType type)
		{
			// exceptions cause the application to stop
			if (type == LogType.Exception)
			{
				// wrapping these in editor checks to ensure it will compile in editor or runtime builds
				#if UNITY_EDITOR
				if (Application.isEditor)
				{
					UnityEditor.EditorApplication.isPlaying = false;
				}
				#endif

				Application.Quit();
			} // errors cause the application to pause in editor or to quit in runtime
			else if (type == LogType.Error)
			{
				if (Application.isEditor)
					Debug.Break();
				else
					Application.Quit();
			}
		}
	}
}

