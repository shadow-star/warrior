﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// For more info look at: https://docs.unity3d.com/Manual/OculusControllers.html
/*
Info Identified for the Oculus GO

Touchpad: Touch point comes through as Axis2D.SecondaryThumbstick_H and Axis2D.SecondaryThumbstick_V
Pressing Touchpad: Comes through as Button.SecondaryThumbstick
Trigger: On/Off button - comes through as Axis1D.SecondaryIndexTrigger

Info Identified for the Oculus Rift
 */

namespace SAE
{
	public class InputDemo : MonoBehaviour 
	{
		public GameObject LeftHand;
		public GameObject RightHand;
		public UnityEngine.UI.Text DebugText;

		// Use this for initialization
		void Start () 
		{
			
		}
		
		// Update is called once per frame
		void Update () 
		{
			if (DebugText == null)
				return;

			string debugText = "";

            debugText += "Model: " + UnityEngine.XR.XRDevice.model + ", ";
			debugText += "Tracking Space: " + UnityEngine.XR.XRDevice.GetTrackingSpaceType() + ", ";
			debugText += "Start Button: " + Input.GetButton("Button.Start");
			debugText += System.Environment.NewLine;

			debugText += "Primary Inputs:" + System.Environment.NewLine;
			debugText += "Trigger: "   + Input.GetAxis("Axis1D.PrimaryIndexTrigger") + ", ";
			debugText += "Thumbstick Button: "   + Input.GetButton("Button.PrimaryThumbstick") + ", ";
			debugText += "Thumbstick: "   + Input.GetAxis("Axis2D.PrimaryThumbstick_H") + ", " + Input.GetAxis("Axis2D.PrimaryThumbstick_V") + ", ";
			debugText += "Thumbrest: " + Input.GetButton("Touch.PrimaryThumbRest") + ", ";
			debugText += "Button 3: " + Input.GetButton("Button.Three") + ", ";
			debugText += "Button 4: " + Input.GetButton("Button.Four");
			debugText += System.Environment.NewLine;;

			debugText += "Primary Inputs:" + System.Environment.NewLine;
			debugText += "Trigger 2: " + Input.GetAxis("Axis1D.SecondaryIndexTrigger") + ", ";
			debugText += "Thumbstick Button: "   + Input.GetButton("Button.SecondaryThumbstick") + ", ";
			debugText += "Thumbstick: "   + Input.GetAxis("Axis2D.SecondaryThumbstick_H") + ", " + Input.GetAxis("Axis2D.SecondaryThumbstick_V") + ", ";
			debugText += "Thumbrest: " + Input.GetButton("Touch.SecondaryThumbRest") + ", ";
			debugText += "Button 1: " + Input.GetButton("Button.One") + ", ";
			debugText += "Button 2: " + Input.GetButton("Button.Two");

			DebugText.text = debugText;
		}
	}
}
