﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class ExperienceInfo
{
	public string LevelName;
	public string InfoText;
}

public class MenuHandler : MonoBehaviour {
	public UnityEngine.UI.Text InfoText;
	public GameObject LaunchButton;

	public ExperienceInfo[] Experiences;

	private int activeExperience = -1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnExperience(int experienceIndex)
	{
		activeExperience = experienceIndex;

		InfoText.text = Experiences[activeExperience].InfoText;
		LaunchButton.SetActive(true);
	}

	public void OnLaunch()
	{
		SceneManager.LoadScene(Experiences[activeExperience].LevelName);
	}
}
