﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : MonoBehaviour
{
	[Header("Movement Variables")]
	public float speed = 1;
	public float rotSpeed = 1;
	public int curNode = 0;
	public float nodeMaxDis = 1;
	public int timeToWait = 5;
	public int score = 1;
	bool canMove = true;

	[Header("Path Locations")]
	public GameObject[] movLoc;

	[Header("Wait Points")]
	public GameObject waitNode1;
	public GameObject waitNode2;

	[Header("Other")]
	public GameObject player;
	public GameManager gm;

	UiManager uiManager;
	AudioClip deathSound;
	AudioSource audioSrc;

	void Start()
	{
		curNode = 0;
		canMove = true;

		movLoc = FindObjectOfType<BatMovementPositions>().batPositions;
		player = FindObjectOfType<Player>().gameObject;
		uiManager = FindObjectOfType<UiManager>();
		gm = FindObjectOfType<GameManager>();

		audioSrc = GetComponent<AudioSource>();
		if (audioSrc == null)
			audioSrc = gameObject.AddComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update()
	{
		if (!uiManager.isPlaying)
		{
			canMove = false;
			return;
		}

		if (canMove)
		{
			if (Vector3.Distance(transform.position, movLoc[curNode].transform.position) < nodeMaxDis)
			{
				if (curNode < movLoc.Length - 1)
				{
					//if(movLoc[curNode] == waitNode1 || movLoc[curNode] == waitNode2)
					if (movLoc[curNode].name.ToLower().Contains("wait"))
						StartCoroutine(WaitPoint());
					curNode += 1;
				}
				else
				{
					curNode = 0;
				}
			}

			if (movLoc[curNode] != null)
			{
				transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, movLoc[curNode].transform.position - transform.position, rotSpeed * Time.deltaTime, 0.0f));
				transform.position += transform.forward * speed * Time.deltaTime;
			}
		}
	}

	IEnumerator WaitPoint()
	{
		canMove = false;
		transform.LookAt(player.transform);
		yield return new WaitForSeconds(timeToWait);
		canMove = true;
	}

	private void OnTriggerEnter(Collider other)
	{
		Debug.Log(other.name);
		if (other.CompareTag("Rock") || other.GetComponent<Sword>() || other.GetComponent<Shield>())
		{
			if (gm == null)
				gm = FindObjectOfType<GameManager>();

			if (other.GetComponent<Gargoyle_Projectile>() != null)
			{
				if (other.GetComponent<Gargoyle_Projectile>().wasHit)
				{
					gm.AddScore(score, transform.position, true);
					Destroy(this.gameObject);
				}
			}
			if(other.GetComponent<Sword>() != null || other.GetComponent<Shield>() != null)
			{
				gm.AddScore(score, transform.position, true);
				Destroy(this.gameObject);
			}
		}
	}

	private void OnDestroy()
	{
		audioSrc.PlayOneShot(deathSound);
	}
}
