﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatMovementPositions : MonoBehaviour
{
    [Header("Path Locations")]
	public GameObject[] batPositions;

    [Header("Wait Points")]
    public GameObject waitNode1;
    public GameObject waitNode2;
}
