﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
This is what spawns the creatures within the environment:
Holds spawn locations if more than 1
Spawns the creature prefabs (2-3 creatures at this point) every so often (random between 10-30 seconds default value)

Spawn Locations
Creature Prefabs
Spawn Timer (editable from inspector)

Spawns creatures from a location every so many seconds
*/

public class CreatureSpawner : MonoBehaviour
{
	public GameObject batSpawnPosition, batPrefab;
	GameObject spawnedBat;
	bool batExists;
	public float spawnDelay;

	GameManager gm;

	void Start()
	{
		gm = FindObjectOfType<GameManager>();
	}

	void Update()
	{
		batExists = spawnedBat == null ? false : true;

		if (!batExists)
		{
			StartCoroutine(SpawnDelay());
		}
	}

	IEnumerator SpawnDelay()
	{
		yield return new WaitForSeconds(spawnDelay);

		//Last minute check if a bat already exists
		spawnedBat = FindObjectOfType<Bat>() == null ? null : FindObjectOfType<Bat>().gameObject;

		//Make absolutely sure that there are no bats before spawning another
		if (!batExists && spawnedBat == null)
			SpawnBat();
	}

	void SpawnBat()
	{
		spawnedBat = Instantiate(batPrefab, batSpawnPosition.transform.position, Quaternion.identity);
		spawnedBat.transform.parent = null;
		spawnedBat.GetComponent<Bat>().gm = gm;
		batExists = true;
	}
}
