﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gargoyle : MonoBehaviour
{
	public GameObject myProjectile;
	public Transform projectileSpawner;

	public Vector3 rigidForce;
	public float attackForce;
	public float attackForceOffset;
	public float deleteDelay;

	AudioSource fireSource;
	public AudioClip fireClip;

	public float fireSpeedEasy, fireSpeedMedium, fireSpeedHard;

	//Animation Speed
	[HideInInspector]
	public float fireSpeed;
	Animator gargoyleAnim;
	UiManager uiManager;

	public float chanceOfRapidfire;
	public int rocksFiredInRapidfire;
	public float delayBetweenRocks;

	Player playerTarget;
	bool canShake;
	Vector3 originalPos;
	ParticleSystem rockPS;

	private void Awake()
	{
		canShake = true;
		fireSpeed = 1;

		fireSource = gameObject.GetComponent<AudioSource>();
		if (fireSource == null)
		{
			fireSource = gameObject.AddComponent<AudioSource>();
			fireSource.clip = fireClip;
		}

		gargoyleAnim = GetComponent<Animator>();
		uiManager = FindObjectOfType<UiManager>();
		playerTarget = FindObjectOfType<Player>();

		rockPS = GetComponentInChildren<ParticleSystem>();
	}

	private void Start()
	{
		originalPos = transform.position;
	}


	public void ProjectileAnimEvent()
	{
		Random.InitState((int)System.DateTime.Now.Ticks);
		float rand = Random.Range(0.0f, 1.0f);

		if (rand < 0.33f)
			fireSpeed = fireSpeedHard;
		else if (rand > 0.33f && rand < 0.66f)
			fireSpeed = fireSpeedMedium;
		else
			fireSpeed = fireSpeedEasy;

		if (canShake)
			canShake = false;

		if (rockPS.isPlaying)
			rockPS.Stop();

		gargoyleAnim.SetFloat("fireSpeed", fireSpeed);
		FireProjectile();
	}

	public void ProjectileAudioEvent()
	{
		fireSource.PlayOneShot(fireClip);
	}

	void FireProjectile()
	{
		Random.InitState(System.DateTime.Today.Millisecond);
		float newAngle = Random.Range(-360.0f, 360.0f);
	
		Random.InitState((int)System.DateTime.Now.Ticks);
		float rapidFireRand = Random.Range(0.0f, 1.0f);

		bool isRapid = rapidFireRand <= chanceOfRapidfire ? true : false;

		projectileSpawner.transform.rotation = Quaternion.Euler(new Vector3(newAngle, newAngle, newAngle));

		int timesToFire;

		if (isRapid)
			timesToFire = rocksFiredInRapidfire;
		else
			timesToFire = 1;

		StartCoroutine(FireRock(isRapid, timesToFire));
	}

	IEnumerator FireRock(bool delay, int timesToFire)
	{
		while (timesToFire > 0)
		{
			GameObject cloneProjectile = Instantiate(myProjectile, projectileSpawner.transform.position, projectileSpawner.transform.rotation);
			Rigidbody rb = cloneProjectile.GetComponent<Rigidbody>();
			Random.InitState(System.DateTime.Now.Millisecond);
			float forceOffset = Random.Range(attackForceOffset, -attackForceOffset);
			rb.AddForce((transform.forward + rigidForce) * (attackForce + forceOffset));
			Destroy(cloneProjectile, deleteDelay);

			if (delay)
				yield return new WaitForSeconds(delayBetweenRocks);

			timesToFire -= 1;

		}
	}

	private void Update()
	{
		if (!uiManager.isPlaying)
			gargoyleAnim.enabled = false;

		if (canShake)
		{
			transform.localPosition = originalPos + Random.insideUnitSphere*0.25f;
		}
	}
}
