﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GargoyleProgression : MonoBehaviour
{
	public GameObject gargoylePrefab;
	GameObject gargoyle1, gargoyle2, gargoyle3;
	public float gargoyleSpawnTimer, firingSpeed;
	private float currentFireSpeed;
	bool canFire;
	Animator gargoyle1_AC, gargoyle2_AC, gargoyle3_AC;

	public List<GameObject> gargoyleSpawnPositions;
	public float initialFireSpeed, normalFireSpeed;

	GameManager gm;

	private void Start()
	{
		gm = FindObjectOfType<GameManager>();
		currentFireSpeed = 1f;

		if (gargoyleSpawnPositions == null)
		{
			Debug.LogError("Failed to get predefined list of Gargoyle spawn positions");
		}
		else
		{
			gargoyle1 = Instantiate(gargoylePrefab, gargoyleSpawnPositions[0].transform.position, gargoyleSpawnPositions[0].transform.rotation);
			StartCoroutine(AttackDifficultyIncrease());
			gargoyleSpawnPositions.RemoveAt(0);
			gm.gargoyles.Add(gargoyle1.GetComponent<Gargoyle>());
		}
	

		StartCoroutine(SpawnEnemies());
	}

	IEnumerator AttackDifficultyIncrease()
	{
		gargoyle1.GetComponent<Gargoyle>().fireSpeed = initialFireSpeed;
		yield return new WaitForSeconds(10.0f);
		gargoyle1.GetComponent<Gargoyle>().fireSpeed = normalFireSpeed;

	}

	IEnumerator SpawnEnemies()
	{
		yield return new WaitForSeconds(gargoyleSpawnTimer);
		if (gargoyle2 == null)
		{
			gargoyle2 = Instantiate(gargoylePrefab, gargoyleSpawnPositions[0].transform.position, gargoyleSpawnPositions[0].transform.rotation);
			gm.gargoyles.Add(gargoyle2.GetComponent<Gargoyle>());
			StartCoroutine(gm.StopFiring());
			gargoyle2.GetComponent<Gargoyle>().fireSpeed = currentFireSpeed;
			gargoyleSpawnPositions.RemoveAt(0);
		}

		yield return new WaitForSeconds(gargoyleSpawnTimer);
		if (gargoyle3 == null)
		{
			gargoyle3 = Instantiate(gargoylePrefab, gargoyleSpawnPositions[0].transform.position, gargoyleSpawnPositions[0].transform.rotation);
			gm.gargoyles.Add(gargoyle3.GetComponent<Gargoyle>());
			StartCoroutine(gm.StopFiring());
			gargoyle3.GetComponent<Gargoyle>().fireSpeed = currentFireSpeed;
			gargoyleSpawnPositions.RemoveAt(0);
		}
	}
}