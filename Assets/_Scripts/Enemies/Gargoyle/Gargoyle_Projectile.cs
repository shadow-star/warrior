﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class Gargoyle_Projectile : MonoBehaviour
{
	public bool wasHit = false;
	GameManager gm;
	ParticleSystem rockParticle;
	public GameObject rockParticleGO;
	public AudioClip rockDestructionClip;
	AudioSource projectileSrc;
	public Vector2 PitchScale;

	private void Start()
	{
		wasHit = false;
		tag = "Rock";
		gm = FindObjectOfType<GameManager>();

		projectileSrc = GetComponent<AudioSource>();

		if (projectileSrc == null)
			projectileSrc = gameObject.AddComponent<AudioSource>();

		projectileSrc.playOnAwake = false;
		projectileSrc.clip = rockDestructionClip;
		rockParticle = GetComponentInChildren<ParticleSystem>();

		projectileSrc.pitch = Random.Range(PitchScale.x, PitchScale.y);
	}

	private void OnCollisionEnter(Collision collision)
	{
		//Check if rock hits another rock
		if (wasHit || collision.gameObject.GetComponent<Sword>() || collision.gameObject.GetComponent<Shield>())
		{
			PlayEffects();
		}

		if (collision.gameObject.GetComponent<Gargoyle_Projectile>() && wasHit)
		{
			gm.AddScore(1, collision.transform.position);
			Destroy(gameObject);
			Destroy(collision.gameObject);
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		//Check if rock hits another rock
		if (wasHit || other.gameObject.GetComponent<Sword>() || other.gameObject.GetComponent<Shield>())
		{
			PlayEffects();
		}

		if (other.gameObject.GetComponent<Gargoyle_Projectile>() && wasHit)
		{
			gm.AddScore(1, other.transform.position);
			Destroy(gameObject);
			Destroy(other.gameObject);
		}
	}
	
	void PlayEffects()
	{
		rockParticle.Play();
		projectileSrc.PlayOneShot(rockDestructionClip);
	}

	private void OnDestroy()
	{
		if (!wasHit)
			gm.rocksHit = 0;
		else
			gm.rocksHit += 1;
	}
}
