﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.XR;
using UnityEngine.SpatialTracking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

/*
Determines if player is alive or dead
Records score
Determines if player is doing well & manages gargoyle spawning
Combo tracking/system

Needs to check if using GO or Rift
If using go, the player needs to be able to switch between shield and sword by using the trigger on the controller. 
If using rift, shield is in left hand and sword is in right

Checks if player has less than 3 lives, ends game and displays score
Keeps track of score
If player is doing well, it will spawn more gargoyle
Checks if player has hit more than 1 rock at a time and applies combo, grants major score bonus
*/

public class GameManager : MonoBehaviour
{
	public GameObject leftHand, rightHand;
	public GameObject goController;
	public GameObject viveController, riftLeft, riftRight, questLeft, questRight;

	[Header("Score")]
	int currentScoreMultiplier;
	int currentScore;
	public int multiplierDivision = 10;
	int timer;
	public int rocksHit;

	public TextMeshProUGUI scoreText;
	bool aiCanAttack;

	//Ui/Gamemode Manager
	UiManager uiManager;

	bool isMenu = false;
	public List<Gargoyle> gargoyles;

	[HideInInspector]
	public bool gargoyleSpawning;

	public GameObject floatingScoreText;

	public float fireDelayTime;

	private void Awake()
	{
		gargoyles = new List<Gargoyle>();
		isMenu = SceneManager.GetActiveScene().name.ToLower().Contains("menu");

		if (isMenu)
			return;

		currentScore = 0;
		scoreText.text = "Score: " + currentScore.ToString();

		uiManager = FindObjectOfType<UiManager>();
	}

	private void Start()
	{
		SwapVRDevice();
	}

	//Checks and swap between Rift/Vive & GO - Omit for Liminal
	void SwapVRDevice()
	{
		//Quest/GO Support
		Debug.Log(Application.isEditor);

		if (Application.isMobilePlatform || Application.isEditor)
			DisableSecondaryController();

		if (XRDevice.model.ToLower().Contains("rift"))
		{
			Instantiate(riftLeft, leftHand.transform);
			Instantiate(riftRight, rightHand.transform);
		}
		else if (XRDevice.model.ToLower().Contains("vive"))
		{
			Instantiate(viveController, leftHand.transform).transform.Rotate(0, 180f, 0);
			Instantiate(viveController, rightHand.transform).transform.Rotate(0, 180f, 0);
		}
	}

	void DisableSecondaryController()
	{
		if (XRDevice.model.ToLower().Contains("quest"))
		{
			Instantiate(questLeft, leftHand.transform);
			Instantiate(questRight, rightHand.transform);
		}
		else
		{
			leftHand.SetActive(false);
			Instantiate(goController, rightHand.transform);
		}
	}

	public GameObject tempFloatingText;

	public void AddScore(int score, Vector3 hitPosition, bool isBat = false)
	{
		currentScore = currentScore + score;
		FindObjectOfType<UiManager>().playerScore.text = "Score: " + currentScore.ToString();
		FindObjectOfType<UiManager>().endGameScore.text = "Score: " + currentScore.ToString();
		if (floatingScoreText)
		{
			if (tempFloatingText == null)
				tempFloatingText = Instantiate(floatingScoreText, hitPosition, Quaternion.identity);
			else
				tempFloatingText.GetComponent<FloatingText>().AddScore(isBat == true ? 10 : 1);
		}	
	}

	public IEnumerator StopFiring()
	{
		foreach (Gargoyle gargoyle in gargoyles)
		{
			gargoyle.gameObject.GetComponent<Animator>().SetFloat("fireSpeed", 0.0f);
		}

		yield return new WaitForSeconds(fireDelayTime);

		foreach (Gargoyle gargoyle in gargoyles)
		{
			gargoyle.gameObject.GetComponent<Animator>().SetFloat("fireSpeed", gargoyle.fireSpeed);
		}
	}
}
