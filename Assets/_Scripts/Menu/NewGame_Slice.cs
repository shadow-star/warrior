﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGame_Slice : MonoBehaviour
{
	Vector3 lockPosition;
	void Start()
	{
		tag = "NewGameRock";
		lockPosition = transform.position;
	}

	private void Update()
	{
		if (transform.position != lockPosition)
			transform.position = lockPosition;
	}
}
