﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame_Slice : MonoBehaviour
{
	Vector3 lockPosition;

	void Start()
	{
		tag = "QuitRock";
		lockPosition = transform.position;
	}

	private void Update()
	{
		if (transform.position != lockPosition)
			transform.position = lockPosition;
	}
}
