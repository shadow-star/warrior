﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using UnityEngine.XR;
using UnityEngine.SpatialTracking;

public class Player : MonoBehaviour
{
	public GameObject rightSword, rightShield;
	public GameObject leftSword, leftShield;

	InputDevices goControllerInput;
	bool buttonPressed = false;

	public AudioClip swapWeaponClip;
	AudioSource audioSrc;

	private void Start()
	{
		rightSword.SetActive(true);
		rightShield.SetActive(false);
		audioSrc = GetComponent<AudioSource>();
		if (audioSrc == null)
			audioSrc = gameObject.AddComponent<AudioSource>();
	}

	void Update()
	{
		if (Application.isMobilePlatform)
			HandleGoInputs();
		if (XRDevice.model.ToLower().Contains("Vive") || XRDevice.model.ToLower().Contains("Rift"))
			HandleRiftInputs();
	}

	void HandleGoInputs()
	{
		//https://tinyurl.com/y4aumhy5 - Oculus Go Controller Layout
		if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger) || OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
		{
			SwapWeaponLeft();
		}
	}

	void HandleRiftInputs()
	{
		if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger) || OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
		{
			SwapWeaponRight();
		}
		if (OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger) || OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
		{
			SwapWeaponLeft();
		}
	}

	void SwapWeaponRight()
	{
		rightSword.SetActive(!rightSword.activeSelf);
		rightShield.SetActive(!rightShield.activeSelf);
		audioSrc.PlayOneShot(swapWeaponClip);
	}

	void SwapWeaponLeft()
	{
		leftSword.SetActive(!leftSword.activeSelf);
		leftShield.SetActive(!leftShield.activeSelf);
		audioSrc.PlayOneShot(swapWeaponClip);
	}
}
