﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
	public ParticleSystem rockParticles;
	public float forwardForce;
	GameManager gm;
	
	private void OnCollisionEnter(Collision collision)
	{
		gm = FindObjectOfType<GameManager>();
		if (collision.gameObject.CompareTag("Rock"))
		{
			if (collision.gameObject.GetComponent<Gargoyle_Projectile>() != null)
			{
				collision.gameObject.GetComponent<Gargoyle_Projectile>().wasHit = true;
				collision.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * forwardForce, ForceMode.VelocityChange);

				gm.AddScore(1, collision.transform.position);
				rockParticles.Play();
			}
		}
	}
}
