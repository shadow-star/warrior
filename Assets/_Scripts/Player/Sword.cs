﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;

public class Sword : MonoBehaviour
{
	Vector3 oldPosition;
	Vector3 velocity;

	public float forceMultiplier;
	public float minimumSliceVelocity = 2.0f;

	public AudioClip destroyClip;
	AudioSource rockDestructionAudioSrc;

	GameManager gm;

	public GameObject splitRock;
	public int score = 1;

	Rigidbody floatingTextRb;

	public Image fadeImg;

	private void Awake()
	{
		rockDestructionAudioSrc = gameObject.GetComponent<AudioSource>();
		if (rockDestructionAudioSrc == null)
		{
			rockDestructionAudioSrc = gameObject.AddComponent<AudioSource>();
			rockDestructionAudioSrc.clip = destroyClip;
		}

		gm = FindObjectOfType<GameManager>();
		if (gm == null)
		{
			Debug.LogError("Failed to find GameManager");
		}
	}

	private void Start()
	{
		oldPosition = transform.position;
	}

	void Update()
	{
		Vector3 newPosition = transform.position;
		Vector3 avgPos = (newPosition - oldPosition);
		oldPosition = newPosition;
		newPosition = transform.position;
		velocity = avgPos / Time.deltaTime;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (!SceneManager.GetActiveScene().name.ToLower().Contains("menu"))
			DestroyRock(other);

		HandleMenu(other);
	}

	void DestroyRock(Collider collision)
	{
		if (collision.gameObject.CompareTag("Rock"))
		{
			if (velocity.magnitude > minimumSliceVelocity)
			{
				if (!Application.isMobilePlatform)
				{
					SliceRock(collision.gameObject);
				}
				else
				{
					GameObject mobileRock = Instantiate(splitRock, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
					Destroy(collision.gameObject);
					foreach (Rigidbody rb in mobileRock.GetComponentsInChildren<Rigidbody>())
					{
						AttackForce(-rb.velocity, rb);
					}
				}
				rockDestructionAudioSrc.PlayOneShot(destroyClip);
				gm.rocksHit += 1;

				//Add score!
				if (!collision.gameObject.GetComponent<Gargoyle_Projectile>().wasHit)
				{
					gm.AddScore(score*2, collision.transform.position);
					collision.gameObject.GetComponent<Gargoyle_Projectile>().wasHit = true;
				}
			}
			else
			{
				AttackForce(-collision.gameObject.GetComponent<Rigidbody>().velocity, collision.gameObject.GetComponent<Rigidbody>());
				if (!collision.gameObject.GetComponent<Gargoyle_Projectile>().wasHit)
				{
					gm.AddScore(score, Vector3.zero);
					collision.gameObject.GetComponent<Gargoyle_Projectile>().wasHit = true;
				}
			}
		}
	}

	void AttackForce(Vector3 direction, Rigidbody rb)
	{
		rb.AddForce(direction * forceMultiplier, ForceMode.Impulse);
	}

	void HandleMenu(Collider collision)
	{
		if (collision.gameObject.CompareTag("NewGameRock"))
		{
			StartCoroutine(NewGameDelay(collision));
		}

		if (collision.gameObject.CompareTag("QuitRock"))
		{
			StartCoroutine(QuitDelay(collision));
		}
	}

	void SliceRock(GameObject parentMesh)
	{
		GameObject[] cutObjects = MeshCut.Cut(parentMesh.gameObject, transform.position, -transform.up, parentMesh.gameObject.GetComponent<MeshRenderer>().material);

		foreach (GameObject obj in cutObjects)
		{
			AddMeshPhysics(obj);
			obj.transform.localScale = parentMesh.gameObject.transform.localScale;

			AttackForce(-obj.gameObject.GetComponent<Rigidbody>().velocity, obj.gameObject.GetComponent<Rigidbody>());
			Destroy(obj, 2.5f);
		}
	}

	void AddMeshPhysics(GameObject mesh)
	{
		if (mesh.GetComponent<CapsuleCollider>() == null && mesh.GetComponent<Rigidbody>() == null)
		{
			mesh.AddComponent<SphereCollider>();
			mesh.AddComponent<Rigidbody>();
		}
	}

	float imgColor;

	[ContextMenu("Test Fade")]
	void FadeTest()
	{
		StartCoroutine(Fade());
	}

	IEnumerator Fade()
	{
		while (fadeImg.color.a < 1f)
		{
			yield return new WaitForSeconds(0.03f);
			fadeImg.color = new Color(0, 0, 0, imgColor);
			imgColor += 0.03f;
		}
	}

	IEnumerator NewGameDelay(Collider collision)
	{
		GameObject mobileRock = Instantiate(splitRock, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
		mobileRock.transform.localScale = new Vector3(0.18f, 0.18f, 0.18f);
		Destroy(collision.gameObject);

		while (fadeImg.color.a < 1f)
		{
			yield return new WaitForSeconds(0.03f);
			fadeImg.color = new Color(0, 0, 0, imgColor);
			imgColor += 0.03f;
		}
		SceneManager.LoadScene(1);
	}

	IEnumerator QuitDelay(Collider collision)
	{
		GameObject mobileRock = Instantiate(splitRock, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
		mobileRock.transform.localScale = new Vector3(0.18f, 0.18f, 0.18f);
		Destroy(collision.gameObject);

		while (fadeImg.color.a < 1f)
		{
			yield return new WaitForSeconds(0.03f);
			fadeImg.color = new Color(0, 0, 0, imgColor);
			imgColor += 0.03f;
		}
		Application.Quit();
	}
}
