﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FloatingText : MonoBehaviour
{
	public float destroyTime = 5f;
	public Rigidbody rb;
	public int score;
	public TextMeshProUGUI scoreText;
	public float upwardsForce;
	public float maxHeight = 1.5f;

	Vector3 currentPosition;
	GameManager gm;

	void Start()
	{
		score = 1;
		Destroy(gameObject, destroyTime);

		gm = FindObjectOfType<GameManager>();
		scoreText = GetComponentInChildren<TextMeshProUGUI>();
		rb = gameObject.GetComponent<Rigidbody>();

		Vector3 direction = new Vector3(Random.Range(-5f, 5f), Random.Range(0f, 5f), Random.Range(0f, 5f));
		float force = Random.Range(-5f, 5f);

		rb.AddForce(transform.up + direction, ForceMode.Impulse);
	}

	private void Update()
	{
		if (score >= 3 && score < 5)
		{
			scoreText.color = Color.yellow;
		}
		else if (score >= 5 && score < 8)
		{
			scoreText.color = Color.red;
		}
		else if (score >= 8 && score < 10)
		{
			scoreText.color = Color.blue;
		}
		else if (score >= 10)
		{
			scoreText.color = Color.magenta;
		}
		else
		{
			scoreText.color = Color.white;
		}

		currentPosition = transform.position;

		if (currentPosition.y > maxHeight)
			currentPosition = new Vector3(transform.position.x, maxHeight, transform.position.z);

		transform.position = currentPosition;
	}

	private void OnDestroy()
	{
		if (gm.tempFloatingText != null)
			if (gm.tempFloatingText == this.gameObject)
				gm.tempFloatingText = null;
	}

	public void AddScore(int scoreToAdd = 1)
	{
		score += scoreToAdd;
		if (scoreText != null)
			scoreText.text = "+" + score.ToString();

		if (rb == null)
		{
			rb = GetComponent<Rigidbody>();

			if (rb == null)
				rb = gameObject.AddComponent<Rigidbody>();
		}

		Vector3 direction = new Vector3(Random.Range(-5f, 5f), Random.Range(0f, 4f), 0f);
		rb.AddForce((transform.up * upwardsForce) + direction, ForceMode.Impulse);
	}
}
