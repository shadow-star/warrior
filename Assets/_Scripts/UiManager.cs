﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiManager : MonoBehaviour
{
    [Header("UI Objects")]
    public GameObject rockButtons;
    public TextMeshProUGUI timerUI;
    public TextMeshProUGUI playerScore;
	public Text endGameScore;
    public GameObject fadePanel;
    public GameObject endGamePanel;

    [Header("Timer Variables")]
    public int minutes;
    public float seconds;

    [Header("Other")]
    public bool isPlaying;

    private void Awake()
	{
        fadePanel.SetActive(true);

        timerUI.text = "0:00";
        isPlaying = true;
    }


    void Update()
	{
        if (rockButtons != null)
            if (rockButtons.activeSelf != true && !isPlaying)
                isPlaying = true;

        if (isPlaying)
		{
			UpdateTimer();
			timerUI.text = UpdateTimerText();
		}
	}

	void UpdateTimer()
	{
		if (minutes <= 0 && seconds <= 0.5)
		{
            endGamePanel.SetActive(true);
            EndGame();
		}

		seconds -= 1 * Time.deltaTime;

		if (seconds < 0f)
		{
			minutes -= 1;
			seconds = 59f;
		}

	}

	string UpdateTimerText()
	{
		//Todo - A better version of this. Its garbage.
		if (minutes == 0)
		{
			return "Time: " + seconds.ToString("F0");
		}
		else
		{
			if (seconds < 9.5f)
			{
				return "Time: " + minutes + ":0" + seconds.ToString("F0");
			}
			else
			{
				return "Time: " + minutes + ":" + seconds.ToString("F0");
			}
		}
	}

	public void EndGame()
	{
		isPlaying = false;
        timerUI.text = "0:00";

        if (rockButtons != null)
            rockButtons.SetActive(true);

    }
}
